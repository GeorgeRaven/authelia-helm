Authelia Helm
=============

Just add water! Batteries included.

Authelia is a single sign on (SSO) provider written in go.

However there are no sufficient helm charts that suit our needs. Thus we create our own from scratch here and update it automatically using CI/CD pipelines.
This helm chart is opinionated. This will install a cluster wide Authelia server and its requirements to a single namespace (whichever is the helm release namespace). This is to keep the expected usages narrow. This will install LLDAP, Redis, and Postgres. Authelia will be stateless and scaleable.

This helm chart will further create some management tools, currently that is PGAdmin. These will be installed in the same namespace, but inaccessible unless you kubectl port-forward to their services (see usage).I also assume that these management tools need to operate independently of the Postgres and LLDAP that they will be managing so they have their own password keys and will be in theory the only exception to the clusters SSO.

All core components (I.E non management tools) have pinned versions. Once this chart is version 1.0.0 it will be semantically versioned while notifying breaking changes on the pinned versions too. So for instance say Authelia needs manual intervention between two of our pinned versions then this charts version will also indicate breaking changes. This way you know before you upgrade this chart if you are to expect any breaking changes.

Installation
++++++++++++

This chart is served right here as a Gitlab helm package.

Adding our helm chart package registry on gitlab.


.. code-block:: bash

   helm repo add authelia-helm-registry https://gitlab.com/api/v4/projects/37423969/packages/helm/stable

.. note::

   The api endpoint is: https://gitlab.com/api/v4/projects/37423969/packages/helm/api/stable/charts but the chart is actually at https://gitlab.com/api/v4/projects/37423969/packages/helm/stable

Ensuring our local index of the helm chart is up to date.

.. code-block:: bash

   helm repo update authelia-helm-registry

Searching our package registry for available versions.

.. code-block:: bash

   helm search repo authelia-helm-registry/authelia --versions

Installing a specific version of the helm chart we would like from our search previously.

.. code-block:: bash

   helm install authelia-helm-registry/authelia --version <MAJOR.MINOR.PATCH>

#OR just install the latest out local index knows about.

.. code-block:: bash

   helm install authelia-helm-registry/authelia

Now lets install everything properly, in its own namespace and with your own values. This command does not enable SMTP as this gives you a simple proof of concept install. Once you are sure this is what you are after you will then need to replace the SMTP details with some of your own beyond this short guide. Most settings you might want to change are at the top of the values.yaml file. The big exception being images and tags.

.. code-block:: bash

   helm install authelia authelia-helm-registry/authelia --version <MAJOR.MINOR.PATCH> --create-namespace --namespace auth --set global.domain.base=<example.org> --set global.domain.full=<auth.example.org> --set global.admin.name=<somebody> --set global.admin.email=<somebody@pm.me>

.. warning::

   It will take some time for Authelia to become ready, in particular it is usually Redis that takes the longest initial setup time. So do not be surprised if it is crash looping because Redis host is not found or unreachable.

By default this proof-of-concept deployment will create randomised passwords and secrets. If you want to take this from PoC to production consider using bitnami sealed-secrets, while disabling secret generation in this chart. That way nothing will start until bitnami creates the secret in the same namespace as Authelia and you can save (while encrypted) the sealed-secret while keeping it git versioned. Please also note one should enable SMTP so that Authelia can be completely stateless, and so users can reset their own passwords.

Usage
+++++

Now that the chart is installed it will do... Nothing. Unless your ingress controller is configured to rely on authelia for authentication everything in your cluster will not be affected.

Each ingress resource is configured individually to listen to Authelia but not Authelias own ingress resource. Please add the following annotations to your ingress-nginx ingress resource to have it listen to Authelia.

.. code-block:: bash

   #Additional annotations necessary to have authelia be an authentication middleware on the nginx proxy.

   annotations:
      nginx.ingress.kubernetes.io/auth-method: GET
      nginx.ingress.kubernetes.io/auth-url: http://authelia.default.svc.cluster.local/api/verify
      nginx.ingress.kubernetes.io/auth-signin: https://auth.example.com?rm=$request_method
      nginx.ingress.kubernetes.io/auth-response-headers: Remote-User,Remote-Name,Remote-Groups,Remote-Email
      nginx.ingress.kubernetes.io/auth-snippet: proxy_set_header X-Forwarded-Method $request_method;

Then based on the authelia configmap which holds authelias configuration, it will control who gets in and who doesnt. This is a simple example assuing the subdomain Authelia is served on is auth.

.. code-block:: bash

   #Example access control section that blocks everything.

   access_control:
     default_policy: deny
     rules:
       ## bypass rule
       - domain:
           - "auth.example.org"
         policy: bypass
       ## catch-all
       - domain:
           - "*.example.org"
         subject:
           - "group:admins"
         policy: one_factor

Export our configuration for inspection.

.. code-block:: bash

   kubectl get cm -n auth authelia-config -o=jsonpath='{.data.configuration\.yml}'

PGAdmin
-------

To access pgadmin use the following commands while replacing ${CHART_NAMESPACE} with whatever namespace you have installed this chart to and ${FORWARD_PORT} to whichever port on your local machine you want it to be available from.

.. code-block:: bash

   # wait for the pgadmin deployment to come alive
   kubectl wait --timeout=600s --for=condition=Available=True -n ${CHART_NAMESPACE} deployment pgadmin-deployment
   # get username / email to log in with
   kubectl -n ${CHART_NAMESPACE} get deployment pgadmin-deployment -o jsonpath="{.spec.template.spec.containers[0].env[0].value}"
   # get the user password
   kubectl -n ${CHART_NAMESPACE} get secret auth -o jsonpath="{.data.pgAdminPassword}" | base64 -d && echo
   # expose pgadmin locked inside the cluster to a port of our choice e.g localhost:8079
   kubectl port-forward svc/pgadmin-service -n ${CHART_NAMESPACE} ${FORWARD_PORT}:http-port

Once logged in you can add the postgres service running in the cluster:

- host: ${CHART_NAMESPACE}-pgsql-hl
- port: 5432
- username: postgres
- password: $(kubectl -n ${CHART_NAMESPACE} get secret auth -o jsonpath="{.data.postgresPassword}" | base64 -d)

Upgrade
+++++++

Upgrade from one version to another explicitly.

.. code-block:: bash

   helm upgrade authelia authelia-helm-registry/authelia --namespace auth --version <MAJOR.MINOR.PATCH>

Uninstall
+++++++++

Uninstall the helm chart and its resources but not anything that you have installed on top.

.. code-block:: bash

   helm uninstall authelia --namespace auth
